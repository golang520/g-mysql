package demo

import (
	"fmt"
	Mysql "gitee.com/golang520/g-mysql"
	"os"
	"testing"
)

func init() {
	mysqlBuilder := Mysql.NewMysqlBuilder("mysql8.0", "127.0.0.1", 3318, "root",
		"3b872161b27e31d0", "gfdb", "utf8", 3)
	mysqlBuilder2 := Mysql.NewMysqlBuilder("mysql5.7", "127.0.0.1", 3317, "root",
		"3b872161b27e31d0", "gfdb", "utf8", 3)
	mysqlPool := Mysql.NewMysqlPool()
	mysqlPool.SetBuilders(mysqlBuilder, mysqlBuilder2)
	err := mysqlPool.Init()
	if err != nil {
		fmt.Println("err", err)
		os.Exit(1)
	}
}

func TestMysql(t *testing.T) {
	tModel := new(TModel)
	err := tModel.FindOne()
	if err != nil {
		fmt.Println(err)
	}

	t2Model := new(T2Model)
	err = t2Model.FindOne()
	if err != nil {
		fmt.Println(err)
	}
}
