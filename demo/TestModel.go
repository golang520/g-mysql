package demo

import (
	"fmt"
	Mysql "gitee.com/golang520/g-mysql"
	"gitee.com/golang520/g-mysql/demo/Field"
)

type TModel struct {
	Mysql.BaseManyModel
}

func (t *TModel) FindOne() error {
	db, err := t.GetDb("mysql8.0")
	if err != nil {
		return err
	}
	var testFiled []*Field.FieldTest
	db.Table("sys_user").Scan(&testFiled)
	for _, v := range testFiled {
		fmt.Println("mysql8.0数据库查询出来的结果为:", v)
	}
	return nil

}
