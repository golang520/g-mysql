package Field

type FieldTest struct {
	UserId   int64  `gorm:"column:user_id"`
	Username string `gorm:"column:username"`
}

func (t FieldTest) TableName() string {
	return "sys_user"
}

func (t *FieldTest) GetId() int64    { return t.UserId }
func (t *FieldTest) GetName() string { return t.Username }
