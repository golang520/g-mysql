package Field

type Field2Test struct {
	UserId   int64  `gorm:"column:user_id"`
	Username string `gorm:"column:username"`
}

func (t Field2Test) TableName() string {
	return "sys_user"
}

func (t *Field2Test) GetId() int64    { return t.UserId }
func (t *Field2Test) GetName() string { return t.Username }
