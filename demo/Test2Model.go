package demo

import (
	"fmt"
	Mysql "gitee.com/golang520/g-mysql"
	"gitee.com/golang520/g-mysql/demo/Field"
)

type T2Model struct {
	Mysql.BaseManyModel
}

func (t *T2Model) Build() *T2Model {
	//t.SetBuilderName("mysql5.7")
	return t
}

func (t *T2Model) FindOne() error {
	db, err := t.Build().GetDb("mysql5.7")
	if err != nil {
		return err
	}
	//db.Raw("select content from " + m.FieldWpCrawlerComment.TableName() + " order by rand() LIMIT 1").Scan(&crawlerComment)
	var testFiled []*Field.Field2Test
	db.Table("sys_user").Scan(&testFiled)
	for _, v := range testFiled {
		fmt.Println("mysql5.7数据库查询出来的结果为:", v)
	}
	return nil
}
