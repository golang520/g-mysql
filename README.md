### g-mysql

#### go-mysql 介绍

- 这库是基于gorm最新版本二次封装
- 同时这个库可以组合任何项目使用

### 具体用法

#### 安装依赖

```shell
go get -u gitee.com/golang520/g-mysql@latest
```

#### 初始化项目
- 注意: 初始化的第一个参数 `buildName` 这个参数必填的,每个库且不能写样的buildName,用于来区分连接的是哪个数据库的实例.
- 注意: 如果初始化了多个数据库后,那么在logic层操作数据库的时候,切记要传递buildName的名字,注意后面的细节,我也会提到的.
- 注意: 你也可以只初始化一个库,实际下面的 `SetBuilders()`方法是可变参数的.
- 注意: 最后一个参数,1表示在控制台打印sql日志,2表示将日志写入文件,3表示不显示日志.
```go
func init() {
	mysqlBuilder := Mysql.NewMysqlBuilder("mysql8.0", "127.0.0.1", 3318, "root", "3b872161b27e31d0", "gfdb", "utf8", true)
	mysqlBuilder2 := Mysql.NewMysqlBuilder("mysql5.7", "127.0.0.1", 3317, "root", "3b872161b27e31d0", "gfdb", "utf8", true)
	mysqlPool := Mysql.NewMysqlPool()
	mysqlPool.SetBuilders(mysqlBuilder, mysqlBuilder2)
	err := mysqlPool.Init()
	if err != nil {
		fmt.Println("err", err)
		os.Exit(1)
	}
}
```
#### 继承结构体
> 注意这个继承是在你已有的机构体里面去继承,这里我们在 TModel 里面继承我们的 Mysql.BaseManyModel
```go
type TModel struct {
	Mysql.BaseManyModel
}
```

#### 获取db实例对象
> 注意: 下面的t.GetDb()可以接受一个参数,或者不接受参数,如果像上面提到的那样,初始化了多个db库,有多个不同的buildName的话,那们在使用t.getDb(buildName)了,如果你只初始化了一个db库,在使用t.getDb()的时候就不用传递buildName参数了!
```go
func (t *TModel) FindOne() error {
    db, err := t.GetDb("mysql8.0")
    if err != nil {
      return err
    }
    var testFiled []*Field.FieldTest
    db.Table("sys_user").Scan(&testFiled)
    for _, v := range testFiled {
        fmt.Println("mysql8.0数据库查询出来的结果为:", v)
    }
    return nil
}

/// 也可以这么写,t.GetDb()不传递参数,,主要是看上面说的那样,你初始化了几个库.
func (t *TModel) FindOne() error {
    db, err := t.GetDb()
}
```

#### 最后看下完整代码
```go
package demo

import (
	"fmt"
	Mysql "gitee.com/golang520/g-mysql"
	"gitee.com/golang520/g-mysql/demo/Field"
)

type TModel struct {
	Mysql.BaseManyModel
}

func (t *TModel) FindOne() error {
	db, err := t.GetDb("mysql8.0")
	if err != nil {
		return err
	}
	var testFiled []*Field.FieldTest
	db.Table("sys_user").Scan(&testFiled)
	for _, v := range testFiled {
		fmt.Println("mysql8.0数据库查询出来的结果为:", v)
	}
	return nil
}
```

### 最后看下我们操作多个数据库实例后的效果图
![](.READM_images/461bd236.png)
